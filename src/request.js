/**
 * Maxymiser Core - Request
 *
 * A handy wrap around `mmcore.CGRequest`.
 *
 * @version 0.1.0
 *
 * @requires mmcore.jQuery
 * @requires mmcore.tryCatch
 *
 * @author evgeniy.pavlyuk@maxymiser.com (Evgeniy Pavlyuk)
 */
(function() {
  'use strict';
  var $ = mmcore.jQuery;
  var ACTION_TRACKING_PAGE_ID = 'event';
  var REQUEST_TIMEOUT = 2250;  // In milliseconds.
  var followingRequest;
  var prepareFollowingRequest = function() {
    followingRequest = $.Deferred();
    mmcore.request.promise = followingRequest.promise();
  };
  /**
   * @param {string=} pageId If not sepcified, `event` is asssumed.
   * @param {boolean=} isSynchronous If not specified, the request will be asynchronous.
   * @return {!Object} Promise.
   */
  mmcore.request = function(pageId, isSynchronous) {
    var request = followingRequest;
    prepareFollowingRequest();
    if (!arguments.length) {
      pageId = ACTION_TRACKING_PAGE_ID;
    }
    mmcore.SetPageID(pageId);
    mmcore._async = !isSynchronous;
    mmcore.CGRequest(request.resolve);  // Callback is called in a try block in a response.
    setTimeout(mmcore.tryCatch(request.reject), REQUEST_TIMEOUT);
    return request.promise();
  };
  prepareFollowingRequest();
}());
